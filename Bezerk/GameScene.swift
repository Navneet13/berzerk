//
//  GameScene.swift
//  Bezerk
//
//  Created by Navpreet Kaur on 2019-10-10.
//  Copyright © 2019 Navneet. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene,SKPhysicsContactDelegate {
    
    //MARK: Outlets for Sprites
    var player:SKSpriteNode!
    let PLAYER_SPEED :CGFloat =  10
        
    override func didMove(to view: SKView) {
        //SKPhysicsContactDelegate setup
        self.physicsWorld.contactDelegate = self
        

         // initialze the player
        self.player = self.childNode(withName: "player") as? SKSpriteNode
        
        //Setup physics for the player
//        self.player.physicsBody = SKPhysicsBody(edgeFrom: , to: )
//
//        self.player.physicsBody?.categoryBitMask = 1
//        self.physicsBody?.categoryBitMask = 2
        
    }

    override func update(_ currentTime: TimeInterval) {
    
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        let nodeA = contact.bodyA.node
        let nodeB = contact.bodyB.node
        if nodeA == nil || nodeB == nil{
            return
        }
        
        print("Collision Detected")
        print("Sprite1: \(nodeA!.name)")
        print("Sprite2: \(nodeB!.name)")
        print("---------------")
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //GET THE MOUSE
        let mouseTouch = touches.first
        if mouseTouch == nil{
            return
        }
        
        
        let location = mouseTouch!.location(in: self)
        
        //WHAT NIDE DID THE MOUSE TOUCH
        let nodeTouched = atPoint(location).name
        print("Player Touched: \(nodeTouched)")
 
        //GAME LOGIC : Move player based on touch
        if nodeTouched == "Up"{
            self.player.position.y = self.player.position.y + PLAYER_SPEED
        }
        if nodeTouched == "Down"{
                   self.player.position.y = self.player.position.y - PLAYER_SPEED
               }
        if nodeTouched == "Left"{
                   self.player.position.x = self.player.position.x - PLAYER_SPEED
               }
        if nodeTouched == "Right"{
                      self.player.position.x = self.player.position.x + PLAYER_SPEED
               }
        
    }
}
